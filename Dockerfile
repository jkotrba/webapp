# base image
FROM node:10.16-alpine

RUN apk add --no-cache bash

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY package*.json ./
RUN npm install

# Copy the rest of the files
COPY . .

# start app
CMD npm start
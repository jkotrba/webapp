import React from 'react'
import authClient from './Auth'

const User = ({ user, onDeleteClick, onRowClicked }) => {
  return (
    <tr onClick={onRowClicked}>
      <td>{user.email}</td>
      <td>{user.handle}</td>
      <td>{new Date(user.created_date).toLocaleDateString()}</td>
      <td>
        {authClient.isAuthenticated() && (
          <button
            type="button"
            className="btn btn-link"
            onClick={event => {
              // Don't cause row selection when clicking the delete button
              event.stopPropagation()
              onDeleteClick()
            }}
          >
            Delete
          </button>
        )}
      </td>
    </tr>
  )
}

const UsersList = ({ users, onDeleteUser, onUserSelected }) => {
  return (
    <div className="users">
      <table className="users-table table">
        <thead>
          <tr>
            <th>Email</th>
            <th>Handle</th>
            <th>Created</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {users.map &&
            users.map(u => (
              <User
                key={u.id}
                user={u}
                onDeleteClick={e => onDeleteUser(u.id)}
                onRowClicked={() => onUserSelected(u)}
              />
            ))}
        </tbody>
      </table>
    </div>
  )
}

export default UsersList

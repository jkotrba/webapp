import React, { useState, useEffect } from 'react'

const NewUserForm = ({ onUserSaved, user = null }) => {
  const email = user != null ? user.email : ''
  const handle = user != null ? user.handle : ''

  const [emailValue, setEmailValue] = useState(email)
  const [handleValue, setHandleValue] = useState(handle)

  const onFormSubmit = async event => {
    event.preventDefault()

    user != null ? await updateUser() : await addUser()

    setEmailValue('')
    setHandleValue('')

    onUserSaved && onUserSaved()
  }

  const updateUser = async () => {
    const userPayload = { email: emailValue, handle: handleValue }
    return fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      referrer: 'no-referrer',
      body: JSON.stringify(userPayload)
    })
  }

  const addUser = () => {
    const userPayload = { email: emailValue, handle: handleValue }
    return fetch(`${process.env.REACT_APP_API_URL}/users/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      referrer: 'no-referrer',
      body: JSON.stringify(userPayload)
    })
  }

  useEffect(() => {
    setEmailValue(email)
    setHandleValue(handle)
  }, [email, handle])

  return (
    <div className="newUserForm">
      <form onSubmit={onFormSubmit}>
        <fieldset className="form-group">
          <label htmlFor="emailTxt">Email</label>
          <input
            id="emailTxt"
            className="form-control"
            value={emailValue}
            onChange={event => setEmailValue(event.target.value)}
          />
        </fieldset>

        <fieldset className="form-group">
          <label htmlFor="handleTxt">Handle</label>
          <input
            id="handleTxt"
            className="form-control"
            value={handleValue}
            onChange={event => setHandleValue(event.target.value)}
          />
        </fieldset>
        <button type="submit" className="btn btn-primary">
          {user != null ? 'Update User' : 'Add User'}
        </button>
      </form>
    </div>
  )
}

export default NewUserForm

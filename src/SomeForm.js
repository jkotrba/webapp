import React, { createRef } from 'react'

class SomeForm extends React.Component {
  constructor(props) {
    super(props)

    // Declare state for holding value of controlled component
    this.state = {
      myinput: ''
    }

    // Declare a reference to the DOM input element on an uncontrolled component
    this.myOtherInput = createRef()
  }

  formSubmit = event => {
    event.preventDefault()

    // Accessing value of controlled component from state
    const myInput = this.state.myinput

    // Accessing value of uncontrolled component from ref variable
    const myOtherInput = this.myOtherInput.current.value

    console.log({ myInput, myOtherInput })
  }

  render() {
    return (
      <form onSubmit={this.formSubmit}>
        <div>
          {/* Declare controlled input component by setting value to state reference and onChange handler */}
          <input
            type="text"
            value={this.state.myinput}
            onChange={event => this.setState({ myinput: event.target.value })}
          />
        </div>
        <div>
          {/* Declare uncontrolled component by setting ref property to ref variable created in constructor */}
          <input type="text" ref={this.myOtherInput} />
        </div>
        <button type="submit">Submit Form</button>
      </form>
    )
  }
}

export default SomeForm
